package rest;


/**
 * Created by manikantad on 23-02-2017.
 */

public class ApiUtils {

    private ApiUtils() {


    }

    public static final String BASE_URL = "http://ec2-54-172-239-158.compute-1.amazonaws.com/";

    //Google
    public static final String MAPS_BASE_URL = "https://maps.googleapis.com/";


    public static ApiInterface getAPIService(int value) {

        if (value == 1) {
            return ApiClient.getClient(BASE_URL).create(ApiInterface.class);
        }
        if (value == 2) {
            return ApiClient.getClient(MAPS_BASE_URL).create(ApiInterface.class);
        }

        return null;
    }
}
