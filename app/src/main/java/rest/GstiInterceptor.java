package rest;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by manikantad on 24-02-2017.
 */

public final class GstiInterceptor implements Interceptor {

    //  Context context = GSTIApplication.getContext();
    public ApiInterface apiInterface;
    // public DBQuery db;

   /* private String android_id = Secure.getString(context.getContentResolver(),
            Secure.ANDROID_ID);*/

    @Override
    public Response intercept(Chain chain)
            throws IOException {
        //  db = new DBQuery(context);
        //  db.createDatabase();

        // String apiToken = getSavedPreferences(context, Constants.PREFS_NAME_API_TOKEN);

        Request request = chain.request();
        request = request.newBuilder()
                .header("Content-Type", "application/json")
                //.header("X-Mashape-Key", "TtQpIZoF5dmshoaKtsdgBHZ3p13Lp1seS8hjsnBUSJ2n1bCYAz")
                .header("Accept", "application/json")
                //.header("deviceOs", OPERATINGSYSTEM)
                .build();

        Response response = chain.proceed(request);


        return response;

    }


}