package rest;

import java.util.ArrayList;

import models.GasStationsModel;
import models.HistoryModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by manikantad on 22-02-2017.
 */

public interface ApiInterface {

    @GET
    Call<GasStationsModel> getNearbyPlaces(@Url String url);

    @GET
    Call<HistoryModel> getHistory(@Url String url);


}



