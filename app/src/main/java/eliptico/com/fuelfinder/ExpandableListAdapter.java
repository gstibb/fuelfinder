package eliptico.com.fuelfinder;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anandbose on 09/06/15.
 */
public class ExpandableListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int HEADER = 0;
    public static final int CHILD = 1;

    private List<Item> data;
    private Context context;


    private RecyclerView mRecyclerView;

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        mRecyclerView = recyclerView;
    }

    public ExpandableListAdapter(List<Item> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;

        switch (type) {
            case HEADER:
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.history_header, parent, false);
                ListHeaderViewHolder header = new ListHeaderViewHolder(view);
                return header;
            case CHILD:
                LayoutInflater childInflator = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = childInflator.inflate(R.layout.history_item, parent, false);
                DefaultRow childRow = new DefaultRow(view);
                return childRow;


        }
        return null;

    }


    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Item item = data.get(position);
        switch (item.type) {
            case HEADER:
                final ListHeaderViewHolder itemController = (ListHeaderViewHolder) holder;
                itemController.refferalItem = item;
                itemController.header_title.setText(item.orderNum);


                itemController.headerLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.invisibleChildren == null) {
                            item.invisibleChildren = new ArrayList<Item>();
                            int count = 0;
                            int pos = data.indexOf(itemController.refferalItem);
                            while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
                                item.invisibleChildren.add(data.remove(pos + 1));
                                count++;
                            }
                            notifyItemRangeRemoved(pos + 1, count);
                            //itemController.btn_expand_toggle.setImageResource(R.drawable.selectactivepickuporders);
                        } else {
                            int pos = data.indexOf(itemController.refferalItem);
                            int index = pos + 1;
                            for (Item i : item.invisibleChildren) {
                                data.add(index, i);
                                index++;
                            }
                            notifyItemRangeInserted(pos + 1, index - pos - 1);
                            mRecyclerView.smoothScrollToPosition(index - 1);
                            //itemController.btn_expand_toggle.setImageResource(R.drawable.selectinactive);
                            item.invisibleChildren = null;
                        }
                    }
                });


                break;
            case CHILD:
                final DefaultRow childControllers = (DefaultRow) holder;

                childControllers.refferalItem = item;
                childControllers.header_title.setText("Price: " + item.orderNum);


                childControllers.address_tv.setText("Date: " + item.address);

                childControllers.eta_tv.setText(item.orderID);

                break;


        }
    }

    //This method sets the status icon based on the status type

    @Override
    public int getItemViewType(int position) {
        return data.get(position).type;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private static class ListHeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        private RelativeLayout headerLayout;
        public Item refferalItem;


        public ListHeaderViewHolder(View itemView) {
            super(itemView);
            header_title = (TextView) itemView.findViewById(R.id.section_title);
            headerLayout = (RelativeLayout) itemView.findViewById(R.id.rl_group_header);

        }
    }

    private static class DefaultRow extends RecyclerView.ViewHolder {
        public TextView header_title, address_tv, eta_tv;
        public Item refferalItem;

        public DefaultRow(View itemView) {
            super(itemView);
            header_title = (TextView) itemView.findViewById(R.id.title);
            address_tv = (TextView) itemView.findViewById(R.id.genre);
            eta_tv = (TextView) itemView.findViewById(R.id.name);
        }
    }

    public static class Item {
        public int type;
        public String orderNum;
        public String address;
        public String orderID;


        public List<Item> invisibleChildren;

        public Item() {

        }

        public Item(int type, String orderNo, String address, String orderID) {
            this.type = type;
            this.orderNum = orderNo;
            this.address = address;
            this.orderID = orderID;

        }

    }


}
