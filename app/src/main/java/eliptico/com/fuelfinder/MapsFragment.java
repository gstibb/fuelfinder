package eliptico.com.fuelfinder;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import base.BaseFragment;
import models.GasStationsModel;
import models.PlacesModel;
import rest.ApiInterface;
import rest.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import util.Constants;
import util.FuelFinderUtil;

import static android.content.Context.LOCATION_SERVICE;


public class MapsFragment extends BaseFragment implements OnMapReadyCallback, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    double latitude;
    double longitude;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private LocationRequest mLocationRequest;
    private ApiInterface apiInterface;
    private boolean gps_enabled = false;
    private boolean network_enabled = false;
    private boolean dialog_enabled = false;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private LocationManager lm;
    private View view;
    private GoogleMap mMap;
    private MapView mapView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_maps, container, false);
        mapView = (MapView) view.findViewById(R.id.map);
        buildGoogleApiClient();
        mapView.onCreate(savedInstanceState);

        if (mapView != null) {
            mapView.getMapAsync(this);
        }
        init();
        return view;
    }

    private void init() {


        lm = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!isLocationAccessAllowed()) {
                requestLocationPermission();
            } else {
                if (!gps_enabled && !network_enabled) {
                    // notify user
                    if (!dialog_enabled) {
                        locationDialog();
                    }
                } else {
                    //Check if Google Play Services Available or not
                    if (!CheckGooglePlayServices()) {
                        Log.d("onCreate", "Finishing test case since Google Play Services are not available");
                        getActivity().finish();
                    } else {
                        Log.d("onCreate", "Google Play Services available.");
                    }


                }
            }
        } else {
            if (!gps_enabled && !network_enabled) {
                // notify user
                if (!dialog_enabled) {
                    locationDialog();
                }
            } else {
                //Check if Google Play Services Available or not
                if (!CheckGooglePlayServices()) {
                    Log.d("onCreate", "Finishing test case since Google Play Services are not available");
                    getActivity().finish();
                } else {
                    Log.d("onCreate", "Google Play Services available.");
                }


            }
        }
    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(getActivity());
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    //We are calling this method to check the permission status
    private boolean isLocationAccessAllowed() {
        //Getting the permission status
        int result = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestLocationPermission() {
        //And finally ask for the permission
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
    }

    @Override
    public void onResume() {

        if (mapView != null) {
            mapView.onResume();
        }
        super.onResume();
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            if (!dialog_enabled) {
                locationDialog();
            }
        }

      /*  if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }*/

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null) {
            mapView.onPause();
        }

        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null) {
            mapView.onLowMemory();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    protected void stopLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        } catch (SecurityException exe) {

        }
    }


    protected void startLocationUpdates() {
        try {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException exe) {

        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                mMap.setMyLocationEnabled(true);
            }
        } else {

            mMap.setMyLocationEnabled(true);
        }


        mMap.clear();
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        getLocation();
    }

    private void getLocation() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");
        String cityName = null;
        try {

            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            cityName = addresses.get(0).getLocality();
        } catch (IOException e) {
            Log.e("Error", e.getMessage() + "");
        }
        showProgressDialog(getActivity(), getString(R.string.search_text));

        getCityResults(cityName, String.valueOf(latitude), String.valueOf(longitude));


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                //denied
                requestLocationPermission();
            } else {
                if (ActivityCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED) {
                    if (!gps_enabled && !network_enabled) {
                        // notify user
                        if (!dialog_enabled)
                            locationDialog();
                    } else {
                        if (!CheckGooglePlayServices()) {
                            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
                            getActivity().finish();
                        } else {
                            Log.d("onCreate", "Google Play Services available.");
                        }


                    }
                } else {
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    Toast.makeText(getActivity(), getString(R.string.enable_loc_permissions), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void getCityResults(String city, String lat, String lng) {


        apiInterface = ApiUtils.getAPIService(1);
        String url = "http://ec2-54-172-239-158.compute-1.amazonaws.com/places/" + city + "/" + lat + "/" + lng;

        Log.e("URL", url);

        apiInterface.getNearbyPlaces(url).enqueue(new Callback<GasStationsModel>() {
            @Override
            public void onResponse(Call<GasStationsModel> call, Response<GasStationsModel> response) {

                if (myDialog != null && myDialog.isShowing())
                    myDialog.dismiss();


                if (response.code() == 200) {
                    displayMaps(response.body());

                } else {
                    Toast.makeText(getActivity(), response.body() + "", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GasStationsModel> call, Throwable t) {
                if (myDialog != null && myDialog.isShowing())
                    myDialog.dismiss();


                // if (!TextUtils.isEmpty(t.getMessage()) && t.getMessage().contains("resolve")) {
                new AlertDialog.Builder(getActivity())

                        .setMessage("Please check your internet connection before launching the app")
                        .setCancelable(false)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        }).show();

            }
        });


    }


    private void displayMaps(final GasStationsModel gasStationsModel) {
        Log.v("Results size", gasStationsModel.getPlaces().length + "");

        for (int i = 0; i < gasStationsModel.getPlaces().length; i++) {

            Double lat = Double.parseDouble(gasStationsModel.getPlaces()[i].getLocation().getLat());
            Double lng = Double.parseDouble(gasStationsModel.getPlaces()[i].getLocation().getLng());
            String placeName = gasStationsModel.getPlaces()[i].getName();
            String vicinity = gasStationsModel.getPlaces()[i].getVendor();
            MarkerOptions markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);

            String levelPatternGson = new Gson().toJson(gasStationsModel.getPlaces()[i]);

            markerOptions.title(levelPatternGson);

            if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.VENDOR_PREFERENCES).equalsIgnoreCase("HP")) {
                if (vicinity.contains("HP") || vicinity.contains("Hindustan")) {

                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.hpicon));

                    mMap.addMarker(markerOptions);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
                }
            } else if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.VENDOR_PREFERENCES).equalsIgnoreCase("IOCL")) {
                if (vicinity.contains("IOCL")) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.indianicon));
                    mMap.addMarker(markerOptions);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
                }
            }


            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {


                    mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(new Gson().fromJson(marker.getTitle(), PlacesModel.class)));

                    return false;
                }
            });

            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    String uri = "http://maps.google.com/maps?daddr=" + marker.getPosition().latitude + "," + marker.getPosition().longitude;
                    Log.e("url", uri);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);
                }
            });
        }

    }


    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId, PlacesModel placesModel) {

        View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.infowindow, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        markerImageView.setImageResource(resId);
        TextView petrolTv = (TextView) customMarkerView.findViewById(R.id.petrol);
        TextView desielTv = (TextView) customMarkerView.findViewById(R.id.desiel);
        TextView openText = (TextView) customMarkerView.findViewById(R.id.isOpen);
        TextView ratingBar = (TextView) customMarkerView.findViewById(R.id.rating);
        petrolTv.setText("P: " + placesModel.getPrice_p().getPrice());
        desielTv.setText("D: " + placesModel.getPrice_d().getPrice());
        if (!placesModel.isOpen_now()) {
            openText.setText("Closed");
            openText.setTextColor(Color.RED);
        } else {
            openText.setText("Open now");
            openText.setTextColor(Color.GREEN);
        }
        ratingBar.setText(placesModel.getRating());

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }


    private void locationDialog() {

        dialog_enabled = true;
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
        dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, 100);
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            lm = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            Location location;

            if (!gps_enabled && !network_enabled) {
                dialog_enabled = false;
                locationDialog();
            } else {
                dialog_enabled = true;
            }

        } else if (requestCode == 101) {

        }

    }

    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private View view;
        LayoutInflater li = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        private PlacesModel placesModel;

        public CustomInfoWindowAdapter(PlacesModel placesModel) {
            view = li.inflate(R.layout.infowindow, null);
            this.placesModel = placesModel;
        }

        @Override
        public View getInfoContents(Marker marker) {

            return view;
        }

        @Override
        public View getInfoWindow(final Marker marker) {
            TextView petrolTv = (TextView) view.findViewById(R.id.petrol);
            TextView desielTv = (TextView) view.findViewById(R.id.desiel);
            TextView openText = (TextView) view.findViewById(R.id.isOpen);
            TextView ratingBar = (TextView) view.findViewById(R.id.rating);
            TextView directionsTv = (TextView) view.findViewById(R.id.directions);
            TextView detailsTv = (TextView) view.findViewById(R.id.details);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String uri = "http://maps.google.com/maps?daddr=" + marker.getPosition().latitude + "," + marker.getPosition().longitude;
                    Log.e("url", uri);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);

                }
            });


            petrolTv.setText("Petrol: " + placesModel.getPrice_p().getPrice());
            desielTv.setText("Diesel: " + placesModel.getPrice_d().getPrice());

            if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.FEUL_PREFERENCES).equalsIgnoreCase("P")) {
                petrolTv.setVisibility(View.VISIBLE);
                desielTv.setVisibility(View.GONE);
            } else if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.FEUL_PREFERENCES).equalsIgnoreCase("D")) {
                petrolTv.setVisibility(View.GONE);
                desielTv.setVisibility(View.VISIBLE);
            } else {
                petrolTv.setVisibility(View.VISIBLE);
                desielTv.setVisibility(View.VISIBLE);
            }

            if (!placesModel.isOpen_now()) {
                openText.setText("Closed");
                openText.setTextColor(Color.RED);
            } else {
                openText.setText("Open now");
                openText.setTextColor(Color.GREEN);
            }
            ratingBar.setText(placesModel.getRating());

            return view;
        }
    }
}

