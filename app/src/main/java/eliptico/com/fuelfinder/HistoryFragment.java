package eliptico.com.fuelfinder;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import base.BaseFragment;
import models.HistoryModel;
import rest.ApiInterface;
import rest.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hithap on 17-11-2017.
 */

public class HistoryFragment extends BaseFragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    private View view;
    private ApiInterface apiInterface;
    private List<ExpandableListAdapter.Item> data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.history_list, null, false);

        showProgressDialog(getActivity(), "Please wait while we are loading history...");
        getHistory("hyderabad");

        init();
        return view;
    }

    private void init() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.history_list);

    }


    private void getHistory(String city) {


        apiInterface = ApiUtils.getAPIService(1);
        String url = ApiUtils.BASE_URL + "history/" + city;

        Log.e("URL", url);

        apiInterface.getHistory(url).enqueue(new Callback<HistoryModel>() {
            @Override
            public void onResponse(Call<HistoryModel> call, Response<HistoryModel> response) {
                if (myDialog != null && myDialog.isShowing())
                    myDialog.dismiss();


                if (response.code() == 200) {


                    setAdapter(response.body());


                } else {
                    Toast.makeText(getActivity(), response.body() + "", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<HistoryModel> call, Throwable t) {
                if (myDialog != null && myDialog.isShowing())
                    myDialog.dismiss();


                // if (!TextUtils.isEmpty(t.getMessage()) && t.getMessage().contains("resolve")) {
                new AlertDialog.Builder(getActivity())

                        .setMessage("Please check your internet connection before launching the app")
                        .setCancelable(false)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();

            }
        });


    }


    private void setAdapter(HistoryModel historyModel) {

        data = new ArrayList<>();
        if (getActivity() == null) return;


        ExpandableListAdapter.Item places = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "HP_Petrol", "", "Group");
        data.add(places);
        for (int i = 0; i < historyModel.getHp_p().length; i++) {

            data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, historyModel.getHp_p()[i].getPrice(), historyModel.getHp_p()[i].getDate(), ""));

        }

        ExpandableListAdapter.Item places1 = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "HP_Diesel", "", "Group");
        data.add(places1);
        for (int i = 0; i < historyModel.getHp_d().length; i++) {

            data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, historyModel.getHp_d()[i].getPrice(), historyModel.getHp_d()[i].getDate(), ""));

        }

        ExpandableListAdapter.Item places2 = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "IOCL_Petrol", "", "Group");
        data.add(places2);

        for (int i = 0; i < historyModel.getIocl_p().length; i++) {

            data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, historyModel.getIocl_p()[i].getPrice(), historyModel.getIocl_p()[i].getDate(), ""));

        }

        ExpandableListAdapter.Item places3 = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "IOCL_Diesel", "", "Group");
        data.add(places3);
        for (int i = 0; i < historyModel.getIocl_d().length; i++) {

            data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, historyModel.getIocl_d()[i].getPrice(), historyModel.getIocl_d()[i].getDate(), ""));

        }






        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        llm.setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(llm);
        mAdapter = new ExpandableListAdapter(data, getActivity());
        mRecyclerView.setAdapter(mAdapter);


    }
}
