package eliptico.com.fuelfinder;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import base.BaseFragment;
import models.GasStationsModel;
import models.PlacesModel;
import rest.ApiInterface;
import rest.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import util.Constants;
import util.FuelFinderUtil;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by hithap on 17-11-2017.
 */

public class FuelListFragment extends BaseFragment implements com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    private LocationRequest mLocationRequest;
    private ApiInterface apiInterface;
    private boolean gps_enabled = false;
    private boolean network_enabled = false;
    private boolean dialog_enabled = false;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private LocationManager lm;
    private View view;
    double latitude;
    double longitude;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.history_list, null, false);
        buildGoogleApiClient();

        init();
        return view;
    }

    private void init() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.history_list);

        lm = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!isLocationAccessAllowed()) {
                requestLocationPermission();
            } else {
                if (!gps_enabled && !network_enabled) {
                    // notify user
                    if (!dialog_enabled) {
                        locationDialog();
                    }
                } else {
                    //Check if Google Play Services Available or not
                    if (!CheckGooglePlayServices()) {
                        Log.d("onCreate", "Finishing test case since Google Play Services are not available");
                        getActivity().finish();
                    } else {
                        Log.d("onCreate", "Google Play Services available.");
                    }


                }
            }
        } else {
            if (!gps_enabled && !network_enabled) {
                // notify user
                if (!dialog_enabled) {
                    locationDialog();
                }
            } else {
                //Check if Google Play Services Available or not
                if (!CheckGooglePlayServices()) {
                    Log.d("onCreate", "Finishing test case since Google Play Services are not available");
                    getActivity().finish();
                } else {
                    Log.d("onCreate", "Google Play Services available.");
                }


            }
        }


    }


    public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
        private GasStationsModel gasStationsModel;


        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, desiel, petrol, rating;
            RelativeLayout layout;

            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.fuelstation_name);
                desiel = (TextView) view.findViewById(R.id.desiel_price);
                petrol = (TextView) view.findViewById(R.id.petrol_price);
                rating = (TextView) view.findViewById(R.id.rating);
                layout = (RelativeLayout) view.findViewById(R.id.fuellist_item);
            }
        }


        public HistoryAdapter(GasStationsModel gasStationsModel) {
            this.gasStationsModel = gasStationsModel;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fuellist_item, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            final PlacesModel placesModel = gasStationsModel.getPlaces()[position];
            String vicinity = gasStationsModel.getPlaces()[position].getVendor();


            holder.title.setText(placesModel.getName());

            holder.desiel.setText("Diesel : " + placesModel.getPrice_d().getPrice());
            holder.petrol.setText("Petrol : " + placesModel.getPrice_p().getPrice());
            holder.rating.setText(placesModel.getRating());

            if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.FEUL_PREFERENCES).equalsIgnoreCase("P")) {
                holder.desiel.setVisibility(View.GONE);
                holder.petrol.setVisibility(View.VISIBLE);

            } else if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.FEUL_PREFERENCES).equalsIgnoreCase("D")) {
                holder.desiel.setVisibility(View.VISIBLE);
                holder.petrol.setVisibility(View.GONE);
            } else {
                holder.desiel.setVisibility(View.VISIBLE);
                holder.petrol.setVisibility(View.VISIBLE);
            }

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String uri = "http://maps.google.com/maps?daddr=" + placesModel.getLocation().getLat() + "," + placesModel.getLocation().getLng();
                    Log.e("url", uri);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);
                }
            });


        }

        @Override
        public int getItemCount() {

            return gasStationsModel.getPlaces().length;
        }
    }


    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(getActivity());
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    //We are calling this method to check the permission status
    private boolean isLocationAccessAllowed() {
        //Getting the permission status
        int result = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestLocationPermission() {
        //And finally ask for the permission
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
    }

    @Override
    public void onResume() {


        super.onResume();
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            if (!dialog_enabled) {
                locationDialog();
            }
        }

      /*  if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }*/

    }

    @Override
    public void onPause() {
        super.onPause();


        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    protected void stopLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        } catch (SecurityException exe) {

        }
    }


    protected void startLocationUpdates() {
        try {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException exe) {

        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");

        mLastLocation = location;


        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");
        String cityName = null;
        try {

            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            cityName = addresses.get(0).getLocality();
        } catch (IOException e) {
            Log.e("Error", e.getMessage() + "");
        }
        showProgressDialog(getActivity(), getString(R.string.search_text));

        getCityResults(cityName, String.valueOf(latitude), String.valueOf(longitude));


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        getLocation();
    }

    private void getLocation() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                //denied
                requestLocationPermission();
            } else {
                if (ActivityCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED) {
                    if (!gps_enabled && !network_enabled) {
                        // notify user
                        if (!dialog_enabled)
                            locationDialog();
                    } else {
                        if (!CheckGooglePlayServices()) {
                            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
                            getActivity().finish();
                        } else {
                            Log.d("onCreate", "Google Play Services available.");
                        }


                    }
                } else {
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    Toast.makeText(getActivity(), getString(R.string.enable_loc_permissions), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void getCityResults(String city, String lat, String lng) {


        apiInterface = ApiUtils.getAPIService(1);
        String url = ApiUtils.BASE_URL + "places/" + city + "/" + lat + "/" + lng;

        Log.e("URL", url);

        apiInterface.getNearbyPlaces(url).enqueue(new Callback<GasStationsModel>() {
            @Override
            public void onResponse(Call<GasStationsModel> call, Response<GasStationsModel> response) {

                if (myDialog != null && myDialog.isShowing())
                    myDialog.dismiss();


                if (response.code() == 200) {
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mAdapter = new HistoryAdapter(response.body());
                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    Toast.makeText(getActivity(), response.body() + "", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GasStationsModel> call, Throwable t) {
                if (myDialog != null && myDialog.isShowing())
                    myDialog.dismiss();


                // if (!TextUtils.isEmpty(t.getMessage()) && t.getMessage().contains("resolve")) {
                new AlertDialog.Builder(getActivity())

                        .setMessage("Please check your internet connection before launching the app")
                        .setCancelable(false)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        }).show();

            }
        });


    }

    private void locationDialog() {

        dialog_enabled = true;
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
        dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, 100);
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            lm = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            Location location;

            if (!gps_enabled && !network_enabled) {
                dialog_enabled = false;
                locationDialog();
            } else {
                dialog_enabled = true;
            }

        } else if (requestCode == 101) {

        }

    }

}
