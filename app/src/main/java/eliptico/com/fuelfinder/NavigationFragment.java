package eliptico.com.fuelfinder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import base.BaseFragment;
import models.GasStationsModel;
import rest.ApiInterface;
import rest.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by manikantad on 24-03-2017.
 */

public class NavigationFragment extends BaseFragment implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private View view;
    private GoogleMap mMap;
    private MapView mapView;
    private LocationRequest lr;
    private GoogleApiClient lc;
    private boolean gps_enabled = false;
    private boolean network_enabled = false;
    private boolean dialog_enabled = false;
    private LocationManager lm;
    private AlertDialog.Builder dialog;
    private ApiInterface apiInterface;


    public static final String TAG = "Activity";
    private double currentLatitude = 0.0, currentLongitude = 0.0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_maps, null, false);

        //OrdersListFragment.showOrders = true;

        lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        mapView = (MapView) view.findViewById(R.id.map);
        getCityResults("Hyderabad", "78.145", "17.456");

        initGoogleAPIClient();

        long INTERVAL = 1000 * 120;
        long FASTEST_INTERVAL = 1000 * 90;

        lr = new LocationRequest();
        lr.setInterval(INTERVAL);
        lr.setFastestInterval(FASTEST_INTERVAL);
        lr.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        mapView.onCreate(savedInstanceState);

        if (mapView != null) {
            mapView.getMapAsync(this);
        }


        return view;
    }

    public void initGoogleAPIClient() {
        lc = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        lc.connect();
    }


    @Override
    public void onResume() {

        if (mapView != null) {
            mapView.onResume();
        }
        super.onResume();
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            if (!dialog_enabled) {
                locationDialog();
            }
        }

        if (lc.isConnected()) {
            startLocationUpdates();
        }

    }

    private void locationDialog() {
        dialog_enabled = true;
        dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
        dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, 100);
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null) {
            mapView.onPause();
        }

        if (lc.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null) {
            mapView.onLowMemory();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!lc.isConnected())
            lc.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        lc.disconnect();
    }

    protected void stopLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(lc, this);
        } catch (SecurityException exe) {

        }
    }


    protected void startLocationUpdates() {
        try {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(lc, lr, this);
        } catch (SecurityException exe) {

        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
    }


    @Override
    public void onLocationChanged(Location location) {


        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        // Assign the new location
        LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        Activity activity = getActivity();

        if (activity != null && isAdded()) {

            if (currentLatLng != null && mMap != null) {

                String cityName = null;
                try {

                    Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    List<Address> addresses = geocoder.getFromLocation(currentLatitude, currentLongitude, 1);
                    cityName = addresses.get(0).getLocality();
                } catch (IOException e) {
                    Log.e("Error", e.getMessage() + "");
                }


                getCityResults(cityName, String.valueOf(currentLatitude), String.valueOf(currentLongitude));

            } else {
                //showToast(getActivity(), getString(R.string.error_location));
            }
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        try {

            Location location = LocationServices.FusedLocationApi.getLastLocation(lc);

            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(lc, lr, this);

            } else {
                //If everything went fine lets get latitude and longitude
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();

                Log.i(TAG, currentLatitude + " WORKS " + currentLongitude);

            }


        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            Log.e(TAG, "Error");
        }


        // startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        lc.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    /**
     * Method to decode polyline points
     * Courtesy : jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */


    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {

            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!gps_enabled && !network_enabled) {
                dialog_enabled = false;
                locationDialog();
            } else {
                dialog_enabled = true;
            }
        }


    }


    private void getCityResults(String city, String lat, String lng) {
        apiInterface = ApiUtils.getAPIService(1);
        String url = "http://ec2-54-172-239-158.compute-1.amazonaws.com/places/" + city + "/" + lat + "/" + lng;

        Log.e("URL", url);

        apiInterface.getNearbyPlaces(url).enqueue(new Callback<GasStationsModel>() {
            @Override
            public void onResponse(Call<GasStationsModel> call, Response<GasStationsModel> response) {

                if (response.code() == 200) {
                    displayMaps(response.body());

                } else {
                    Toast.makeText(getActivity(), response.body() + "", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GasStationsModel> call, Throwable t) {


                // if (!TextUtils.isEmpty(t.getMessage()) && t.getMessage().contains("resolve")) {
                new AlertDialog.Builder(getActivity())

                        .setMessage("Please check your internet connection before launching the app")
                        .setCancelable(false)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();

            }
        });


    }


    private void displayMaps(GasStationsModel gasStationsModel) {
        Log.v("Results size", gasStationsModel.getPlaces().length + "");

        for (int i = 0; i < gasStationsModel.getPlaces().length; i++) {

            Double lat = Double.parseDouble(gasStationsModel.getPlaces()[i].getLocation().getLat());
            Double lng = Double.parseDouble(gasStationsModel.getPlaces()[i].getLocation().getLng());
            String placeName = gasStationsModel.getPlaces()[i].getName();
            String vicinity = gasStationsModel.getPlaces()[i].getVendor();
            MarkerOptions markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);


            if (vicinity.contains("HP")) {

                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.hpicon, gasStationsModel.getPlaces()[i].getPrice_p().getPrice(), gasStationsModel.getPlaces()[i].getPrice_d().getPrice())));

            } else if (vicinity.contains("IOCL")) {

                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.indianicon, gasStationsModel.getPlaces()[i].getPrice_p().getPrice(), gasStationsModel.getPlaces()[i].getPrice_d().getPrice())));
            }


            mMap.addMarker(markerOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(12));

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    String uri = "http://maps.google.com/maps?daddr=" + marker.getPosition().latitude + "," + marker.getPosition().longitude;
                    Log.e("url", uri);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);


                    return false;
                }
            });
        }

    }


    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId, String petrol, String desiel) {

        View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.infowindow, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        markerImageView.setImageResource(resId);
        TextView petrolTv = (TextView) customMarkerView.findViewById(R.id.petrol);
        TextView desielTv = (TextView) customMarkerView.findViewById(R.id.desiel);
        petrolTv.setText("P: " + petrol);
        desielTv.setText("D: " + desiel);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }
}
