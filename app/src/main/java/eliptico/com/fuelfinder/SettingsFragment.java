package eliptico.com.fuelfinder;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import base.BaseFragment;
import util.Constants;
import util.FuelFinderUtil;

/**
 * Created by hithap on 17-11-2017.
 */

public class SettingsFragment extends BaseFragment {
    private View view;
    private RadioGroup fuelRG, vendorRG, uiRG;
    private RadioButton hpRB, ioclRB, mapRB, listRB, desielRB, petrolRB, bothRB;
    private String fuelPre = "Both", vendorPref = "IOCL", uiPref = "MAP";
    private Button saveBtn;
    private CheckedTextView maxresTV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.settings, null, false);
        init();
        return view;
    }

    private void init() {
        fuelRG = (RadioGroup) view.findViewById(R.id.fuel_group);
        vendorRG = (RadioGroup) view.findViewById(R.id.vendor_group);
        uiRG = (RadioGroup) view.findViewById(R.id.ui_group);
        saveBtn = (Button) view.findViewById(R.id.saveBtn);
        maxresTV = (CheckedTextView) view.findViewById(R.id.maxrestv);

        desielRB = (RadioButton) view.findViewById(R.id.desielradio);
        petrolRB = (RadioButton) view.findViewById(R.id.petrolradio);
        hpRB = (RadioButton) view.findViewById(R.id.hpradio);
        ioclRB = (RadioButton) view.findViewById(R.id.ioclradio);
        mapRB = (RadioButton) view.findViewById(R.id.mapradio);
        listRB = (RadioButton) view.findViewById(R.id.listradio);
        bothRB = (RadioButton) view.findViewById(R.id.bothfuel);

        if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.FEUL_PREFERENCES).equalsIgnoreCase("P")) {
            petrolRB.setChecked(true);
            fuelPre = "P";
        } else if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.FEUL_PREFERENCES).equalsIgnoreCase("D")) {
            desielRB.setChecked(true);
            fuelPre = "D";
        } else {
            bothRB.setChecked(true);
        }
        if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.VENDOR_PREFERENCES).equalsIgnoreCase("HP")) {
            hpRB.setChecked(true);
            vendorPref = "HP";
        } else {

            ioclRB.setChecked(true);
            vendorPref = "IOCL";
        }
        if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.UI_PREFERENCES).equalsIgnoreCase("MAP")) {
            mapRB.setChecked(true);
            uiPref = "MAP";
        } else {
            listRB.setChecked(true);
            uiPref = "LIST";
        }
        if (FuelFinderUtil.getSavedPreferences(getActivity(), Constants.MAXRESULTS).equalsIgnoreCase("true")) {
            maxresTV.setChecked(true);
        } else {
            maxresTV.setChecked(false);
        }

        setonclickListener();
    }

    private void setonclickListener() {
        fuelRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.desielradio:
                        fuelPre = "D";
                        break;
                    case R.id.petrolradio:
                        fuelPre = "P";
                        break;
                    case R.id.bothfuel:
                        fuelPre = "Both";
                        break;
                }

            }
        });
        vendorRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.hpradio:
                        vendorPref = "HP";
                        break;
                    case R.id.ioclradio:
                        vendorPref = "IOCL";
                        break;
                }

            }
        });
        uiRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.mapradio:
                        uiPref = "MAP";
                        break;
                    case R.id.listradio:
                        uiPref = "LIST";
                        break;
                }

            }
        });
        maxresTV.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((CheckedTextView) v).toggle();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FuelFinderUtil.savePreferences(getActivity(), Constants.FEUL_PREFERENCES, fuelPre);
                FuelFinderUtil.savePreferences(getActivity(), Constants.VENDOR_PREFERENCES, vendorPref);
                FuelFinderUtil.savePreferences(getActivity(), Constants.UI_PREFERENCES, uiPref);
                if (maxresTV.isSelected()) {
                    FuelFinderUtil.savePreferences(getActivity(), Constants.MAXRESULTS, "true");
                } else {
                    FuelFinderUtil.savePreferences(getActivity(), Constants.MAXRESULTS, "false");
                }

                if (!TextUtils.isEmpty(uiPref) && uiPref.equalsIgnoreCase("MAP")) {
                    MapsFragment mapsFragment = new MapsFragment();
                    ((MainActivity) getActivity()).replaceFragment(mapsFragment, false);
                } else {
                    FuelListFragment fuelListFragment = new FuelListFragment();
                    ((MainActivity) getActivity()).replaceFragment(fuelListFragment, false);
                }
            }
        });
    }
}
