package base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import eliptico.com.fuelfinder.R;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by hithap on 03-03-2017.
 */

public class BaseActivity extends AppCompatActivity {
    public ProgressDialog myDialog;
    public ImageView imgSetting, imgBack;
    private TextView titltTv;
    private View view;
    public View toolbarDivider;

    private static final String TAG = "UploadService";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Fabric.with(this, new Crashlytics());

        //   init();
//        setOnclickListener();
    }


    /*This method is used to initilize*/


    private void setOnclickListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                removeFragment();
            }
        });

    }


    public ProgressDialog showProgressDialog(Context context, String text) {
        myDialog = new ProgressDialog(context);
        myDialog.setMessage(text);
        myDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        myDialog.setCancelable(false);
        myDialog.show();
        return myDialog;
    }


    /**
     * This method to replace a fragment in through out the app
     */
    public void replaceFragment(Fragment frag, boolean addToBackStack) {
        try {

            String backStateName = frag.getClass().getName();
            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

            if (!fragmentPopped && manager.findFragmentByTag(frag.getClass().getSimpleName()) == null) { //fragment not in back stack, create it.
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.content_frame, frag, frag.getClass().getSimpleName());
                if (addToBackStack)
                    transaction.addToBackStack(frag.getClass().getSimpleName());
                transaction.commitAllowingStateLoss();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        } catch (Exception e) {

        }
    }

    /**
     * This method removes the Fragment from backstack
     */

    public void removeFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();

            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            /*if (currentFragment instanceof ConfirmPickUpOrderFragment || currentFragment instanceof FragmentDeliveryOrderList || currentFragment instanceof FragmentTransferOrderList) {
                removeAllFragments();
            }*/


        } else {
            finish();
        }

    }


    public void removeAllFragments() {
        try {
            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            Log.e("replace error", e.getMessage() + "replace fragment error");
        }

    }





    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onUserInteraction() {

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(
                    INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);

        } catch (Exception e) {
            System.out.println("Exception in savenote" + e.toString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myDialog != null && myDialog.isShowing()) {
            myDialog.dismiss();
            myDialog = null;
        }
    }


}
