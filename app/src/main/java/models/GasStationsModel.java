package models;

/**
 * Created by hithap on 18-10-2017.
 */

public class GasStationsModel {


    private PlacesModel[] places;

    public PlacesModel[] getPlaces() {
        return places;
    }

    public void setPlaces(PlacesModel[] places) {
        this.places = places;
    }

    @Override
    public String toString() {
        return "ClassPojo [places = " + places + "]";
    }
}



