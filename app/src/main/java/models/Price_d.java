package models;

/**
 * Created by hithap on 18-10-2017.
 */

public class Price_d {

    private String price;

    private String date;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ClassPojo [price = " + price + ", date = " + date + "]";
    }

}
