package models;

/**
 * Created by hithap on 18-10-2017.
 */

public class PlacesModel {
    private Location location;

    private String vendor;

    private String name;

    private Price_d price_d;

    private Price_P price_p;
    private boolean open_now;
    private String rating;

    public boolean isOpen_now() {
        return open_now;
    }

    public void setOpen_now(boolean open_now) {
        this.open_now = open_now;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Price_d getPrice_d() {
        return price_d;
    }

    public void setPrice_d(Price_d price_d) {
        this.price_d = price_d;
    }

    public Price_P getPrice_p() {
        return price_p;
    }

    public void setPrice_p(Price_P price_p) {
        this.price_p = price_p;
    }

    @Override
    public String toString() {
        return "ClassPojo [location = " + location + ", vendor = " + vendor + ", name = " + name + ", price_d = " + price_d + ", price_p = " + price_p + "]";
    }
}
