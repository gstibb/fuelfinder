package models;

/**
 * Created by hithap on 24-11-2017.
 */

public class HistoryModel {


    private HP_p[] hp_p;

    private HP_d[] hp_d;

    private Iocl_d[] iocl_d;

    private Iocl_p[] iocl_p;

    public HP_p[] getHp_p() {
        return hp_p;
    }

    public void setHp_p(HP_p[] hp_p) {
        this.hp_p = hp_p;
    }

    public HP_d[] getHp_d() {
        return hp_d;
    }

    public void setHp_d(HP_d[] hp_d) {
        this.hp_d = hp_d;
    }

    public Iocl_d[] getIocl_d() {
        return iocl_d;
    }

    public void setIocl_d(Iocl_d[] iocl_d) {
        this.iocl_d = iocl_d;
    }

    public Iocl_p[] getIocl_p() {
        return iocl_p;
    }

    public void setIocl_p(Iocl_p[] iocl_p) {
        this.iocl_p = iocl_p;
    }

    @Override
    public String toString() {
        return "ClassPojo [hp_p = " + hp_p + ", hp_d = " + hp_d + ", iocl_d = " + iocl_d + ", iocl_p = " + iocl_p + "]";
    }


}
