package util;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by hithap on 12-05-2017.
 */

public class FuelFinderUtil {

    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;
    public static final String PREFS_NAME = "GSTI_CLS";


    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }


    public static void savePreferences(Context context, String key, String value) {

        sharedPreferences = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public static String getSavedPreferences(Context context, String key) {

        String value;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        value = sharedPreferences.getString(key, "");

        return value;
    }

    public static void removePreferences(Context context, String key) {

        sharedPreferences = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }


}
