package util;

import android.util.Log;

/**
 * Created by hithap on 16-03-2017.
 */

public class LogUtil {
    private static final boolean mShowLog = true;

    public static void d(String tag, String msg) {
        if (msg == null || msg.length() == 0) {
            return;
        }

        if (mShowLog) {
            Log.d(tag, msg);
        }
    }

    public static void e(String tag, String msg, Exception e) {
        if (msg == null || msg.length() == 0) {
            return;
        }

        if (mShowLog) {
            Log.e(tag, msg, e);
        }
    }

    public static void e(String tag, String msg) {
        if (msg == null || msg.length() == 0) {
            return;
        }

        if (mShowLog) {
            Log.e(tag, msg);
        }
    }
}
